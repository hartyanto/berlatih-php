<?php
function tukar_besar_kecil($string){
//kode di sini
$abjad = "abcdefghijklmnopqrstuvwxyz !";
$abjadKapital = strtoupper("abcdefghijklmnopqrstuvwxyz ");
$tukarBesarKecil = "";

    for( $i = 0; $i < strlen($string); $i++ ){
        for( $j = 0; $j < strlen($abjad); $j++ ){
                if( $string[$i] == $abjad[$j] ){
                    $tukarBesarKecil = $tukarBesarKecil . strtoupper($string[$i]);
                    break;
                } else if( $string[$i] == $abjadKapital[$j] ) {
                    $tukarBesarKecil = $tukarBesarKecil . strtolower($string[$i]);
                    break;
                }
                else if( is_numeric($string[$i]) || $string[$i] == "-" ) {
                    $tukarBesarKecil = $tukarBesarKecil . $string[$i];
                break;
                }
        }
    } 

    echo "<br>";
    return $tukarBesarKecil;
}
// Menggunakan ASCII Number
// ASCII Number untuk a - z adalah 97 - 122
// ASCII Number untuk A - Z adalah 65 - 90
// ASCII Number untuk space dan simbol lain(!,-,+, dll) adalah 32 - 64
function tukar_besar_kecilASCII($string){
$tukarBesarKecil = "";

    for( $i = 0; $i < strlen($string); $i++ ){
        for( $j = 32; $j <= 122; $j++ ) {
            if( $j <= 64 && $string[$i] == chr($j) ){
                $tukarBesarKecil = $tukarBesarKecil . chr($j);
            } else if( $j <= 90 && $j >= 65 && $string[$i] == chr($j)){
                $tukarBesarKecil = $tukarBesarKecil . strtolower(chr($j));
            } else if( $j >= 97 && $j <= 122 && $string[$i] == chr($j)){
                $tukarBesarKecil = $tukarBesarKecil . strtoupper(chr($j));
            }
        }
    }
    echo "<br>";
    return $tukarBesarKecil;
}

// Menggunakan ctype_lower dan ctype_upper
// ctype_lower dan ctype_upper mengembalikan nilai boolean
function tukar_besar_kecilCtype($string){
$tukarBesarKecil = "";

    for( $i = 0; $i < strlen($string); $i++ ){
        $nilai = ctype_lower($string[$i]);
        if( $nilai ){
            $tukarBesarKecil = $tukarBesarKecil . strtoupper($string[$i]);
        } else {
            $tukarBesarKecil = $tukarBesarKecil . strtolower($string[$i]);
        }
    }
    
    echo "<br>";
    return $tukarBesarKecil;
}

// TEST CASES
echo tukar_besar_kecil('Hello World'); // "hELLO wORLD"
echo tukar_besar_kecil('I aM aLAY'); // "i Am Alay"
echo tukar_besar_kecil('My Name is Bond!!'); // "mY nAME IS bOND!!"
echo tukar_besar_kecil('IT sHOULD bE me'); // "it Should Be ME"
echo tukar_besar_kecil('001-A-3-5TrdYW'); // "001-a-3-5tRDyw"
echo "<br><br>Menggunakan ASCII Number :";
echo tukar_besar_kecilASCII('Hello World'); // "hELLO wORLD"
echo tukar_besar_kecilASCII('I aM aLAY'); // "i Am Alay"
echo tukar_besar_kecilASCII('My Name is Bond!!'); // "mY nAME IS bOND!!"
echo tukar_besar_kecilASCII('IT sHOULD bE me'); // "it Should Be ME"
echo tukar_besar_kecilASCII('001-A-3-5TrdYW'); // "001-a-3-5tRDyw"
echo "<br><br>Menggunakan ctype :";
echo tukar_besar_kecilCtype('Hello World'); // "hELLO wORLD"
echo tukar_besar_kecilCtype('I aM aLAY'); // "i Am Alay"
echo tukar_besar_kecilCtype('My Name is Bond!!'); // "mY nAME IS bOND!!"
echo tukar_besar_kecilCtype('IT sHOULD bE me'); // "it Should Be ME"
echo tukar_besar_kecilCtype('001-A-3-5TrdYW'); // "001-a-3-5tRDyw"

?>