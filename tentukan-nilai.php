<?php
function tentukan_nilai($number) {
    //  kode disini
    $kategoriNilai = "";
    if( $number <= 100 && $number >= 85 ) {
        $kategoriNilai = "Sangat Baik <br>";
    } else if( $number < 85 && $number >= 70 ) {
        $kategoriNilai = "Baik <br>";
    } else if( $number < 70 && $number >= 60 ) {
        $kategoriNilai = "Cukup <br>";
    } else if( $number < 60 ) {
        $kategoriNilai = "Kurang <br>";
    }
    return $kategoriNilai;
}


//TEST CASES
echo tentukan_nilai(98); //Sangat Baik
echo tentukan_nilai(76); //Baik
echo tentukan_nilai(67); //Cukup
echo tentukan_nilai(43); //Kurang
?>