<?php
function ubah_huruf($string){
//kode di sini
    $abjad = "abcdefghijklmnopqrstuvwxyz";
    $plusSatu = "";

    for( $i = 0; $i < strlen($string); $i++ ){
        for( $j = 0; $j < (strlen($abjad) - 1 ); $j++){
            if( $string[$i] == $abjad[$j] ) {
                $plusSatu = $plusSatu . $abjad[$j + 1];
            }
        }

        if( $string[$i] == $abjad[25] ){           //khusus untuk z menjadi a
            $plusSatu = $plusSatu . $abjad[0];
        }
    }
    echo "<br>";
    return $plusSatu;
}

// Menggunakan ASCII Number
// ASCII Number untuk a - z adalah 97 - 122
function ubah_hurufASCII($string) {
    $plusSatu = "";

    for( $i = 0; $i < strlen($string); $i++ ){
        for( $j = 97; $j <= 122; $j++ ){
            if($string[$i] == chr($j) ) {
                if( $string[$i] == chr(122)){           //khusus untuk z menjadi a
                    $plusSatu = $plusSatu . chr(97);
                } else {
                    $plusSatu = $plusSatu . chr($j + 1);
                }
            }
        }

    }
    echo "<br>";
    return $plusSatu;
}

// TEST CASES
echo ubah_huruf('wow'); // xpx
echo ubah_huruf('developer'); // efwfmpqfs
echo ubah_huruf('laravel'); // mbsbwfm
echo ubah_huruf('keren'); // lfsfo
echo ubah_huruf('semangat'); // tfnbohbu
echo ubah_huruf('zaza'); // abab
echo "<br><br> Menggunakan ASCII Number";
echo ubah_hurufASCII('semangat'); // tfnbohbu
echo ubah_hurufASCII('wow'); // xpx
echo ubah_hurufASCII('developer'); // efwfmpqfs
echo ubah_hurufASCII('zaza'); // abab

?>