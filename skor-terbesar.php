<?php
function skor_terbesar($arr){
//kode di sini
$tempNilaiLaravel = [];
$tempLaravel = [];
$tempNilaiReactNative = [];
$tempReactNative = [];
$tempNilaiReactJS = [];
$tempReactJS = [];

    for( $i=0 ; $i < count($arr); $i++ ){
        if ( $arr[$i]["kelas"] == "Laravel" ) {
            array_push($tempNilaiLaravel, $arr[$i]["nilai"]);
            array_push($tempLaravel, $arr[$i]);
        }
        if ( $arr[$i]["kelas"] == "React Native" ) {
            array_push($tempNilaiReactNative, $arr[$i]["nilai"]);
            array_push($tempReactNative, $arr[$i]);
        }
        if ( $arr[$i]["kelas"] == "React JS" ) {
            array_push($tempNilaiReactJS, $arr[$i]["nilai"]);
            array_push($tempReactJS, $arr[$i]);
        }
    }

    rsort($tempNilaiLaravel);
    foreach( $tempLaravel as $sLaravel ){
        if( $sLaravel["nilai"] == $tempNilaiLaravel[0] ) {
            $laravel = $sLaravel;
        }
    }
    rsort($tempNilaiReactNative);
    foreach( $tempReactNative as $rNative ){
        if( $rNative["nilai"] == $tempNilaiReactNative[0] ) {
            $reactNative = $rNative;
        }
    }
    rsort($tempNilaiReactJS);
    foreach( $tempReactJS as $rJS ){
        if( $rJS["nilai"] == $tempNilaiReactJS[0] ) {
            $reactJS = $rJS;
        }
    }

    $output = [
        $laravel["kelas"] => $laravel,
        $reactNative["kelas"] => $reactNative,
        $reactJS["kelas"] => $reactJS
    ];

    return $output;
    
}

// TEST CASES
$skor = [
  [
    "nama" => "Bobby",
    "kelas" => "Laravel",
    "nilai" => 78
  ],
  [
    "nama" => "Regi",
    "kelas" => "React Native",
    "nilai" => 86
  ],
  [
    "nama" => "Aghnat",
    "kelas" => "Laravel",
    "nilai" => 90
  ],
  [
    "nama" => "Indra",
    "kelas" => "React JS",
    "nilai" => 85
  ],
  [
    "nama" => "Yoga",
    "kelas" => "React Native",
    "nilai" => 77
  ],
];

print_r(skor_terbesar($skor));
/* OUTPUT
  Array (
    [Laravel] => Array
              (
                [nama] => Aghnat
                [kelas] => Laravel
                [nilai] => 90
              )
    [React Native] => Array
                  (
                    [nama] => Regi
                    [kelas] => React Native
                    [nilai] => 86
                  )
    [React JS] => Array
                (
                  [nama] => Indra
                  [kelas] => React JS
                  [nilai] => 85
                )
  )
*/